# Maintainer: 
# Contributor: Dan Johansen
# Contributor: Bart De Vries <bart at mogwai dot be>
# Contributor: Jai-JAP <jai.jap.318@gmail.com>

pkgname=(
  box64
#  box64-apple-silicon
)
pkgbase=box64
pkgver=0.3.4
pkgrel=1
pkgdesc="Linux Userspace x86_64 Emulator with a twist"
arch=('aarch64' 'x86_64')
url="https://box86.org"
license=('MIT')
depends=('gcc-libs')
makedepends=(
  'cmake'
  'python'
)
optdepends=('gl4es: OpenGL 2 for GLES 2 devices')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/ptitSeb/box64/archive/v${pkgver}.tar.gz")
sha256sums=('081224cd27f5fa67fe4e9034ebbe0076e1da51b0f11bf84b67494752c7090d72')

build() {
  if [ $CARCH == "aarch64" ]; then
    cmake -B build -S "${pkgbase}-${pkgver}" \
          -DARM64='ON' \
          -DARM_DYNAREC='ON' \
          -DNOGIT='ON' \
          -DNO_LIB_INSTALL='ON' \
          -DCMAKE_BUILD_TYPE='RelWithDebInfo' \
          -DCMAKE_INSTALL_PREFIX='/usr' \
        -Wno-dev
    cmake --build build
  fi

  if [ $CARCH == "x86_64" ]; then
    cmake -B build -S "${pkgbase}-${pkgver}" \
          -DLD80BITS='1' \
          -DNOALIGN='1' \
          -DNOGIT='ON' \
          -DNO_LIB_INSTALL='ON' \
          -DCMAKE_BUILD_TYPE='RelWithDebInfo' \
          -DCMAKE_INSTALL_PREFIX='/usr' \
        -Wno-dev
    cmake --build build
  fi


#  cmake -B build-apple-silicon -S "${pkgbase}-${pkgver}" \
#        -DM1='ON' \
#        -DNOGIT='ON' \
#        -DNO_LIB_INSTALL='ON' \
#        -DCMAKE_BUILD_TYPE='RelWithDebInfo' \
#        -DCMAKE_INSTALL_PREFIX='/usr' \
#        -Wno-dev
#  cmake --build build-apple-silicon

  # Build manpage
  cd "${pkgbase}-${pkgver}"
  pod2man --stderr "docs/${pkgbase}.pod" > "docs/${pkgbase}.1"
}

package_box64() {
  if [ $CARCH == "aarch64" ]; then
    DESTDIR="${pkgdir}" cmake --install build
  fi

  if [ $CARCH == "x86_64" ]; then
    # Install manually as cmake_install doesn't seem to work on x86_64
    install -Dm755 "build/${pkgbase}" -t "$pkgdir/usr/bin/"
    pushd "${pkgbase}-${pkgver}"
    sed 's:${CMAKE_INSTALL_PREFIX}/bin/${BOX64}:/usr/bin/box64:' \
      < system/box64.conf.cmake > system/box64.conf
    install -Dm644 "system/${pkgbase}.conf" -t "$pkgdir/etc/binfmt.d/"
    install -Dm644 "system/${pkgbase}.${pkgbase}rc" -t "$pkgdir/etc/"
    popd
  fi

  cd "${pkgbase}-${pkgver}"
  install -Dm644 "docs/${pkgbase}.1" -t "$pkgdir/usr/share/man/man1/"
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}

package_box64-apple-silicon() {
  pkgdesc+=" (Apple Silicon version)"
  provides=('box64')
  conflicts=('box64')

  DESTDIR="${pkgdir}" cmake --install build-apple-silicon

  cd "${pkgbase}-${pkgver}"
  install -Dm644 "docs/${pkgbase}.1" -t "$pkgdir/usr/share/man/man1/"
  install -Dm644 LICENSE -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}
